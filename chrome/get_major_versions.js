const fs = require('fs');

const getMajor = function(version) {
    return version.split('.')[0];
}

const fileContent = fs.readFileSync('versions.txt').toString();
const versions = fileContent.split('\n');
let majorVersions = [];
for(let i=0;i<versions.length-1;i++){
    if(getMajor(versions[i]) !== getMajor(versions[i+1])) {
        majorVersions.push(versions[i]);
    }
}
majorVersions.push(versions[versions.length-1]);

fs.writeFileSync("major_versions.txt", majorVersions.join('\n'));

