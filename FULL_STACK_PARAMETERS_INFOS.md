# Full stack parameters infos

All the elements to know about the device
These elements must be passed to the URL of the page to be stored along the fingerprint attributes.

- Device manufacturer: `sudo dmidecode | grep -A3 '^System Information' | grep -oP 'Manufacturer:\ \K(.*)$'`
- Device product name: `sudo dmidecode | grep -A3 '^System Information' | grep -oP 'Product Name:\ \K(.*)$'`
- GPU manufacturer: `sudo lshw -C display | grep -oP 'vendor: \K(.*)$'`
- GPU model: `sudo lshw -C display | grep -oP 'product: \K(.*)$'`
- OS name: `cat /etc/os-release | grep -oP '^ID=\K(.*)$'`
- OS version: `cat /etc/os-release | grep -oP '^VERSION_ID="\K([^"]*)'`
- Processor manufacturer: `cat /proc/cpuinfo | grep -oPm 1 'vendor_id[[:space:]]+: \K(.*)$'`
- Processor model: `cat /proc/cpuinfo | grep -oPm 1 'model name[[:space:]]+: \K(.*)$'`
- Locale: with locale command (check `get_infos.sh`)
- Battery: Check the `/sys/class/power_supply/BAT0` folder