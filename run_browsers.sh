#!/bin/bash

# Example of use:
# ./run_browsers.sh -b chrome,firefox -u http://localhost:8092/environnement_controle
#

# The list of browsers on which we will run the xp, separated by a comma. Accepted values: chrome,firefox,edge,chrome,brave,opera
browsers=""

# Run the xp in normal mode, incognito mode, or both. Default is normal mode only. Usage: put either, default incognito, or the string default,incognito
incognitoModes=("")

# The url to visit when starting the browser
url=""

# The time the browser will stay on the page before closing. Default is 10 seconds. Might increase the number if some results take some time to be collected
time=10

# The profiles used to run the browser(s). If you run with several browsers, be sure the profile names are the same for all. Profile does not mean we will set a full profile, but we'll rather change some configuration options (see relevant files in browsers configuration folders)
profiles=()

# Repeat several times the same configuration/combination of characteristics. Default is 1
repeat=1

# Run on associated nightly versions. Only available for Firefox
nightly="false"

# Get associated beta versions. Only available for Firefox
beta=""

# Only useful for nightly. Help narrow the research of the corresponding versions to a specific set of years
years=""


while getopts "gnei:b:t:u:p:r:y:" opt; do
  case $opt in
    g) sudo-g5k
    sudo apt -qq -y install libdbus-glib-1-2
    ;;
    n) nightly="true"
    ;;
    e) beta="-e"
    ;;
    i) IFS=',' read -r -a incognitoModes <<< "$OPTARG"
    ;;
    b) IFS=',' read -r -a browsers <<< "$OPTARG"
    ;;
    t) time=$OPTARG
    ;;
    u) url=$OPTARG
    ;;
    p) IFS=',' read -r -a profiles <<< "$OPTARG"
    ;;
    r) repeat=$OPTARG
    ;;
    y) years="-y $OPTARG"
    ;;
  esac
done

uname=$(uname -s)

platform="linux"

if [[ $uname == "Darwin" ]]; then
  platform="macos"
  # install brew
  # ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
  # brew install --build-from-source coreutils

fi

echo "cd scripts/${platform}"

cd scripts/$platform

for browser in "${browsers[@]}"
do
    echo "------------------"
    echo "Run for ${browser}"
    for incognito in "${incognitoModes[@]}"
    do
        if [[ ${#profiles[@]} = 0 ]]; then
            for i in $(seq $repeat); do
                echo "./run_${browser}.sh -i $incognito -t ${time} -u ${url} ${beta}"
                ./run_${browser}.sh -i $incognito -t ${time} -u ${url} ${beta}
            done
        else
            for profile in "${profiles[@]}"
            do
                for i in $(seq $repeat); do
                    echo "./run_${browser}.sh -i $incognito -t ${time} -u ${url} -p ${profile} ${beta}"
                    ./run_${browser}.sh -i $incognito -t ${time} -u ${url} -p ${profile} ${beta}
                done
            done
        fi
    done

    if [[ ${browser} = "firefox" &&  ${nightly} = "true" ]]; then
        for i in $(seq $repeat); do
            echo "./run_nightly.sh $incognito -t ${time} -u ${url} ${years}"
            ./run_nightly.sh $incognito -t ${time} -u ${url} ${years}
        done
    fi

done



cd ..