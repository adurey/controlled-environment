# Commands

## Pre-experiment

### Firefox 

#### Get all FF versions

```
curl -s "https://ftp.mozilla.org/pub/firefox/releases/" | grep -oP 'releases\/\K([0-9]*\.([0-9]|b|rc)*(\.[0-9])*)' > firefox_versions.txt
```

### Chromium

#### Get all Chromium versions

```
curl -S "https://omahaproxy.appspot.com/history.json?channel=stable&os=linux" | jq -r '.[] | .version' > chromium_versions.txt
```
