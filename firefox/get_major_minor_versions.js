const fs = require('fs');

const fileContent = fs.readFileSync('all_versions.txt').toString();
const versions = fileContent.split('\n');
let majorMinorVersions = [];
for(let i=0;i<versions.length-1;i++){
    const version = versions[i];
    let isVersion = true;
    for(let i=0;i<version.length;i++) {
        if(version[i] !== '.' && (version[i] > '9' || version[i] < '0')) {
            isVersion = false;
        }
    }
    if(isVersion)
        majorMinorVersions.push(version);
    else
        console.log(version)

}
majorMinorVersions.push(versions[versions.length-1]);

fs.writeFileSync("major_minor_versions.txt", majorMinorVersions.join('\n'));

