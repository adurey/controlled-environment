#!/bin/bash

parameters=""
while IFS= read -r line
do
  parameters=${parameters}${line}"&"
done < "infos_browser_stack.txt"

encodedParameters=`./scripts/linux/url_encode.sh "$parameters"`

echo "http://spirals-fingerkit.lille.inria.fr/webGLDataAA?${encodedParameters}"