# Browser settings

## Firefox

Set resistFingerprint to `true` and `false`

## Brave

Set fingerprinting defense to `disabled`, `standard` and `strict`