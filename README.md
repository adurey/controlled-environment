# Controlled environment

## Summary

This repository allows you to run a set of browsers with specific versions and/or configuration to visit a page on the Web.

## Things implemented

Only on Linux for the moment

- Run Firefox:
  - Release, beta and nightly versions
  - Specific configuration
  - Private mode
- Run Chrome:
  - Release version (from v76)
- Run Brave:
  - Current version
- Run Edge:
  - Current version
- Run Opera:
  - Current version

What is implemented but is buggy (does not work):
- Run Tor
- Run beta version of Chrome
- Run specific profile on Brave
- Run things on MacOS (need to check)
- Run things on Windows (need to check)

## How to run

### Get the env

Run the script `scripts/<your env>/get_infos.sh`.
You need admin rights.
It will create a file at the root of the repository named `infos.txt`

If you're running inside:
- a virtual machine, add the `-v` option
- a Docker container, add the `-d` option

In both cases, the file name will be `infosVirtual.txt`

### Start the experiment


Main file is `run_browsers.sh`

#### Command line

It takes the following parameters:
- `-g`: indicates it runs on g5k
- `-i <value>`: Run normal/incognito mode, or both. Value can be `i` for incognito mode, or `i,` for both normal and incognito mode. By defaut, only normal mode is run.
- `-b <value>`: The browsers to run.
  Each browser you desire to run must have a `run_<browser>.sh` file in the corresponding `scripts/<env>` folder.
  Values are separated by a comma.
  Example: `-b chrome,firefox,opera`
- `-t <value>`: The time on which the browser will run.
  Default is 10 seconds.
  Use this if you need experiments that need some time to run.
  Take into consideration the time needed by the browser to start (few seconds or more)
- `-u <value>`: The URL to visit
- `-p <value>`: Profile(s) to run. Only for Firefox. Check `firefox/profiles`
- `-r <value>`: Number of times each experiment will be run.
  Can be used to mesure the stability of your experiment.
  Default is `1`.
- `-n`: indicates you also want to run on nightly versions (useful only for Firefox)
- `-e`: indicates you also want to run on beta versions (useful only for Firefox)
- `-y <value>`: Years, separated by a comma to help searching for nightly versions (if no set, it can be very long to find for nightly versions)
  Example: `-y 2017,2018,2019`
    
Examples of valid command:

```
./run_browsers.sh -u http://localhost:8080/test1.html -b firefox -t 20 -p default,restrictive -en -y 2018
./run_browsers.sh -u http://localhost:8080/test2.html -b chrome,brave -t 20 -i i,
```

### Additional data

For Chrome and Firefox, it is possible to specify the versions to run.
These information are not read from CLI but from the `versions.txt` file on `firefox/` and `chrome/` folders.
Check `firefox/all_versions.txt`, `firefox/major_minor_versions.txt`, `firefox/major_versions.txt`, `chrome/available_versions.txt` and `chrome/major_versions.txt` for more informations.
