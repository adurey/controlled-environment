Param(
  [Parameter(HelpMessage="Normal mode or incognito mode")]
    [switch]$i,

  [Parameter(HelpMessage="Time the browser has to stay open")]
    [int]$t,

  [Parameter(HelpMessage="Profile name")]
    [string]$p,

  [Parameter(HelpMessage="URL")]
    [string]$u

)

$incognito=""
$time=10
$rootFolder=".."
$dataFolder="${rootFolder}/firefox"
$infoHostFile="${rootFolder}/infos.txt"
$infoVirtualFile="${rootFolder}/infosVirtual.txt"
$profile=$p
$url=$u



if($t -ne "") {
  $time=$t
}


if($i.IsPresent) {
  $incognito="-private-window"
}


$language="en-US"


$platform="win32"

$currentPlatform=(Get-WMIObject Win32_OperatingSystem).OSArchitecture
if($currentPlatform -eq "64 bits") {
  $platformm="win64"
}

# Get options
$versionsFile="${dataFolder}/versions.txt"
$overrideFile="${dataFolder}/override.ini"
$extractFolder="${rootFolder}/firefox-tmp"
$executable="./${extractFolder}/firefox/firefox"

if(!(Test-Path -Path $extractFolder )){
  New-Item $extractFolder -ItemType "directory"
}

$uninstallExe='C:\Program Files\Mozilla Firefox\uninstall\helper.exe'


if(Test-Path -Path 'C:\Program Files (x86)\Mozilla Firefox\uninstall\helper.exe'){
  $uninstallExe='C:\Program Files (x86)\Mozilla Firefox\uninstall\helper.exe'
}

echo "Uninstalling existing version"
Start-Process -Wait -FilePath $uninstallExe -ArgumentList '/S','/v','/qn' -passthru

# Put profile in the right directory
#$rootDirectory=$env+":APPDATA\Mozilla\Firefox\Profiles\"
#if( $profile -ne "" ) {
#  fullProfileName=$(ls -1 profiles | findstr $profile)
#  fullProfilePath=profiles/${fullProfileName}
#  New-Item ${rootDirectory} -ItemType "directory"
#  Copy-Item -Path $fullProfilePath -Destination $rootDirectory -Recurse
#}

$parameters=""
foreach($line in Get-Content $infoHostFile) {
  $parameters=$parameters + $line + "&"
}

if(!(Test-Path -Path $extractFolder )){
  foreach($line in Get-Content $infoVirtualFile) {
    $parameters=$parameters + $line + "&"
  }
}
$parameters=$parameters + "BROWSER_NAME=FIREFOX&BROWSER_PROFILE=" + $profile + "&"


foreach($version in Get-Content $versionsFile) {
  $echoVersion = "Run for version " + $version 
  echo $echoVersion

  $fullParameters=$parameters + "BROWSER_VERSION=" + $version

  $url="https://ftp.mozilla.org/pub/firefox/releases/" + $version + "/" + $platform + "/" + $language + "/Firefox%20Setup%20" + $version + ".exe"
  $filename = "firefox-" + $version + ".exe"
  wget $url -OutFile $filename

  # encodedParameters=`./url_encode.sh "$fullParameters"`


  Start-Process -Wait -FilePath $filename -ArgumentList '/S','/v','/qn' -passthru

  #  -new-instance -no-remote -width 1024 -height 768 -override $overrideFile $incognito $url
  #  timeout ${time}s ${executable} -new-instance -no-remote -width 1024 -height 768 -override ${overrideFile} ${incognito} ${url}?${encodedParameters}
  $executable='C:\Program Files\Mozilla Firefox\firefox.exe'


  if(Test-Path -Path 'C:\Program Files (x86)\Mozilla Firefox\'){
    $executable='C:\Program Files (x86)\Mozilla Firefox\firefox.exe'
    Copy-Item -Path ".\override.ini" -Destination "C:\Program Files (x86)\Mozilla Firefox\browser\"
    Copy-Item -Path ".\mozilla.cfg" -Destination "C:\Program Files (x86)\Mozilla Firefox\"
    Copy-Item -Path ".\local-settings.js" -Destination "C:\Program Files (x86)\Mozilla Firefox\defaults\pref"
  }

  if(Test-Path -Path "C:\Program Files\Mozilla Firefox\"){
    Copy-Item -Path ".\override.ini" -Destination "C:\Program Files\Mozilla Firefox\browser\"
    Copy-Item -Path ".\mozilla.cfg" -Destination "C:\Program Files\Mozilla Firefox\"
    Copy-Item -Path ".\local-settings.js" -Destination  "C:\Program Files\Mozilla Firefox\defaults\pref"
  }

  $rootDirectory=$env+":APPDATA\Mozilla\Firefox\Profiles\"
  cat $rootDirectory

  if( $profile -ne "" ) {
    fullProfileName=$(ls -1 profiles | findstr $profile)
    fullProfilePath=profiles/${fullProfileName}
    New-Item ${rootDirectory} -ItemType "directory"
    Copy-Item -Path $fullProfilePath -Destination $rootDirectory -Recurse
  } else {
    Start-Process -FilePath $executable  -ArgumentList "-CreateProfile","test"
  }

  echo "  Everything is ready. Starting the browser..."
  Start-Process -FilePath $executable 'http://spirals-fingerkit.lille.inria.fr/environnement_controle'
  Start-Sleep -Seconds 30
  Stop-Process -Name 'Firefox'

  $uninstallExe='C:\Program Files\Mozilla Firefox\uninstall\helper.exe'

  if(Test-Path -Path 'C:\Program Files (x86)\Mozilla Firefox\uninstall\helper.exe'){
    $uninstallExe='C:\Program Files (x86)\Mozilla Firefox\uninstall\helper.exe'
  }

  echo "Cleaning..."
  Start-Process -Wait -FilePath $uninstallExe -ArgumentList '/S','/v','/qn' -passthru

}
# Start-Process -FilePath 'C:\Program Files (x86)\Mozilla Firefox\firefox.exe'