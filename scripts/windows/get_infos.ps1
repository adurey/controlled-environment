Param(
	[Parameter(HelpMessage="Is the script run in a virtual machine")]
    [switch]$v,

	[Parameter(HelpMessage="Is the script run in a docker container")]
    [switch]$d
)

$virtualFilename="infosVirtual.txt"
$virtualPrefix="VIRTUAL_"

$filename="infos.txt"
$prefix=""
$virtualization=""


If ($v.IsPresent) {
	$filename=$virtualFilename
	$prefix=$virtualPrefix
	$virtualization="VIRTUAL_MACHINE"
    
} ElseIf ($d.IsPresent) {
	$filename=$virtualFilename
	$prefix=$virtualPrefix
	$virtualization="DOCKER"
}

Remove-Item $filename

$model=wmic computersystem get model /value | findstr '='
Add-Content $filename $prefix"DEVICE_PRODUCT"=$($model.Split('=')[1])

$manufacturer=wmic computersystem get manufacturer /value | findstr '='
Add-Content $filename $prefix"DEVICE_MANUFACTURER"=$($manufacturer.Split('=')[1])

$graphicCards=wmic path Win32_VideoController get Name /value | Select-String -Pattern '='
$graphicCards=$graphicCards -split '\n'
[int]$count=1
Foreach($graphicCard in $graphicCards) {            
    Add-Content $filename $prefix"GPU_MODEL_"$count=$($graphicCard.Split('=')[1])
  	$count++;
}

$graphicManufacturers=wmic path Win32_VideoController get AdapterCompatibility /value | Select-String -Pattern '='
$graphicManufacturers=$graphicManufacturers -split '\n'
[int]$count=1
Foreach($graphicManufacturer in $graphicManufacturers) {            
    Add-Content $filename $prefix"GPU_MODEL_"$count=$($graphicManufacturer.Split('=')[1])
  	$count++;
}


$audioCards = get-wmiobject -class "Win32_SoundDevice"
[int]$count=1
 
foreach ($audioCard in $audioCards) { 
	
	$name=$(echo $audioCard.Name) 
	Add-Content $filename $prefix"AUDIO_CARD_MODEL_"$count=$name
	$count++;
} 

$webcams = Get-PnpDevice -FriendlyName "*webcam*"
[int]$count=1
 
foreach ($webcam in $webcams) { 
	$name=$(echo $webcam.FriendlyName) 
	Add-Content $filename $prefix"VIDEO_INPUT_CARD_MODEL_"$count=$name
	$count++;
} 

$osName=wmic os get caption /value | findstr '='
Add-Content $filename $prefix"OS_NAME"=$($osName.Split('=')[1])


$osVersion=wmic os get caption /value | findstr '='
Add-Content $filename $prefix"OS_VERSION"=$($osVersion.Split('=')[1])


$cpuManufacturer=wmic cpu get Manufacturer /value | findstr '='
Add-Content $filename $prefix"PROCESSOR_MANUFACTURER"=$($cpuManufacturer.Split('=')[1])

$cpuName=wmic cpu get Name /value | findstr '='
Add-Content $filename $prefix"PROCESSOR_MODEL"=$($cpuName.Split('=')[1])

$locale=GET-WinSystemLocale
Add-Content $filename $prefix"LOCALE"=$locale.Name

Add-Content $filename $prefix"HOSTNAME"=$(hostname)

if ($virtualization -ne "") {
  Add-Content $filename "VIRTUALIZATION"=$virtualization
}
