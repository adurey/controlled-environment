#/bin/bash

virtualFilename="infosVirtual.txt"
virtualPrefix="VIRTUAL_"
virtualization=""

filename="infos.txt"
prefix=""
# d for docker, v for virtual machine
while getopts "vd" opt; do
  case $opt in
    v) filename=$virtualFilename
      prefix=$virtualPrefix
      virtualization="VIRTUAL_MACHINE"
      ;;
    d) filename=$virtualFilename
      prefix=$virtualPreafix
      virtualization="DOCKER"
  esac
done

rm -f ${filename}

echo $prefix"DEVICE_MANUFACTURER=Apple" >> ${filename}
echo $prefix"DEVICE_PRODUCT=$(curl -s https://support-sp.apple.com/sp/product?cc=$( system_profiler SPHardwareDataType | awk '/Serial/ {print $4}' | cut -c 9- ) | sed 's|.*<configCode>\(.*\)</configCode>.*|\1|')" >> ${filename}
count=1

# Get GPU manufacturers
while IFS= read -r manufacturer; do
  echo $prefix"GPU_MANUFACTURER_${count}=$(echo $manufacturer | sed 's|.*Vendor:[[:space:]]\(.*\)|\1|')" >> ${filename}
  count=$((count+1))
done < <(system_profiler SPDisplaysDataType | grep 'Vendor')

count=1

# Get GPU models
while IFS= read -r model; do
  echo $prefix"GPU_MODEL_${count}=${model}" >> ${filename}
  count=$((count+1))
done < <(system_profiler SPDisplaysDataType | grep '^[[:space:]]\{4\}[A-Za-z'])

count=1

# Get audio cards
while IFS= read -r audio; do
  echo $prefix"AUDIO_CARD_MODEL_${count}=$audio" >> ${filename}
  count=$((count+1))
done < <(system_profiler SPAudioDataType | grep '^[[:space:]]\{4\}[A-Za-z'] | grep -v "Devices:")

# Get video input ?
while IFS= read -r video; do
  echo $prefix"VIDEO_INPUT_${count}=$(echo $video | sed "s/    //" | sed "s/://")" >> ${filename}
  count=$((count+1))
done < <(system_profiler SPCameraDataType | grep "^    [^ ]")

echo $prefix"OS_NAME=$(sw_vers | grep "ProductName" | sed 's|.*ProductName:[[:space:]]\(.*\)|\1|')" >> ${filename}
echo $prefix"OS_VERSION=$(sw_vers | grep "ProductVersion" | sed 's|.*ProductVersion:[[:space:]]\(.*\)|\1|')" >> ${filename}
echo $prefix"PROCESSOR_MANUFACTURER=$(sysctl -n machdep.cpu.brand_string)" >> ${filename}
echo $prefix"PROCESSOR_MODEL=$(sysctl -n machdep.cpu.brand_string)" >> ${filename}

echo $prefix"LOCALE=$(curl -s https://support-sp.apple.com/sp/product?cc=$( system_profiler SPHardwareDataType | awk '/Serial/ {print $4}' | cut -c 9- ) | sed 's|.*<locale>\(.*\)</locale>.*|\1|')" >> ${filename}

echo $prefix"HOSTNAME=$(hostname)" >> ${filename}

if [[ $virtualization -ne "" ]]; then
  echo "VIRTUALIZATION=$virtualization" >> ${filename}
fi
# Screen size
# https://superuser.com/questions/603528/how-to-get-the-current-monitor-resolution-or-monitor-name-lvds-vga1-etc
# IFS='x' read -a size <<< $(xdpyinfo | grep dimensions | sed -r 's/^[^0-9]*([0-9]+x[0-9]+).*$/\1/')
# echo "SCREEN_WIDTH=${size[0]}" >> ${filename}
# echo "SCREEN_HEIGHT=${size[1]}" >> ${filename}
