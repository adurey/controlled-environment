#!/bin/bash

# Check for options i (run browser in incognito mode), t (timeout duration), p (profile folder), d (data folder), u (url)
incognito=""
time=10
rootFolder=".."
dataFolder="${rootFolder}/firefox"
infoHostFile="${rootFolder}/infos.txt"
infoVirtualFile="${rootFolder}/infosVirtual.txt"
profile=""
url=""
while getopts "it:p:o:u:" opt; do
  case $opt in
    i) incognito="-private-window"
      ;;
    t) time=$OPTARG
      ;;
    p) profile=$OPTARG
      ;;
    u) url=$OPTARG
      ;;
  esac
done

echo '-----------'
echo $url

# Get language
language="en-US"

# Get platform
platform="mac"

# Get options
versionsFile="${dataFolder}/versions.txt"
overrideFile="${dataFolder}/override.ini"
mozillaCfgFile="${dataFolder}/profiles/${profile}/mozilla.cfg"
localSettingsFile="${dataFolder}/local-settings.js"
extractFolder="${rootFolder}/firefox-tmp"
executable="./${extractFolder}/firefox/firefox"

# Put profile in the right directory
localProfilesDirectory="/Users/spirals/Library/Caches/Firefox/Profiles/"
profilesDirectory="/Users/spirals/Library/Application Support/Firefox/Profiles/"
rm -rf "${profilesDirectory}*"
rm -rf "${localProfilesDirectory}*"

# Format URL parameters
parameters=""
while IFS= read -r line
do
  parameters=${parameters}${line}"&"
done < "$infoHostFile"
while IFS= read -r line
do
  parameters=${parameters}${line}"&"
done < "$infoVirtualFile"
parameters=${parameters}"BROWSER_NAME=FIREFOX&BROWSER_PROFILE="${profile}"&"

# Remove any existing Firefox version
while IFS= read -r line
do
  sudo rm -rf $line
done <<< "$(mdfind -name 'Firefox' | grep 'Application.*/Firefox')"

# Iterate on Firefox versions
while IFS= read -r version
do

  echo "Run for version ${version}"
  fullParameters=${parameters}"BROWSER_VERSION="${version}

  firefoxVersion="Firefox%20${version}"
  filenamePkg="${firefoxVersion}.pkg"
  firefoxPkgRenamed="Firefox-${version}.pkg"

  fileURL="https://ftp.mozilla.org/pub/firefox/releases/${version}/${platform}/${language}/${filenamePkg}"
  echo $fileURL
  curl -q "${fileURL}" -o $firefoxPkgRenamed
  echo "  File downloaded from ${fileURL}"

  installer -pkg $firefoxPkgRenamed -target CurrentUserHomeDirectory

  # Copy config files

  appPath="/Users/$(id -un)/Applications/Firefox.app/Contents"

  cp $mozillaCfgFile "$appPath/Resources/autoconfig.cfg"
  cp $localSettingsFile "$appPath/Resources/defaults/pref/autoconfig.js"

  # Setup profile
  if [[ $profile != "" ]]; then
    fullProfileName=$(ls -1 profiles | grep $profile)
    fullProfilePath="profiles/"${fullProfileName}
    cp -r ${fullProfilePath} "${profilesDirectory}"
    p=$profile
  else
    p="profile-${version}"
    open -a "$appPath/MacOS/firefox" --args -CreateProfile ${p}
  fi

  cp ${mozillaCfgFile} ${extractFolder}/firefox/

  encodedParameters=`./url_encode.sh "$fullParameters"`

  echo "  Everything is ready. Starting the browser..."

  echo $url
  open -a "$appPath/MacOS/firefox" --args ${url}?$encodedParameters -P $p
#  ${executable} -new-instance -no-remote -width 1024 -height 768 -override ${overrideFile} ${incognito} http://localhost:8092/environnement_controle?${encodedParameters}
  sleep ${time}s
  osascript -e 'tell application "FireFox"
  quit
  end tell'
  echo "  Browser closed after ${time} seconds "
  echo "  Cleaning..."

  # Clean
  sleep 2
  while IFS= read -r line
  do
    sudo rm -rf $line
  done <<< "$(mdfind -name 'Firefox' | grep 'Application.*/Firefox')"
done < "$versionsFile"

rm -rf ${extractFolder}