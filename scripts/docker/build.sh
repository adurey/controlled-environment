#!/bin/bash
# Usage: ./build_docker.sh <tag> <version>
# Ex : ./build_docker.sh ubuntu20_04 latest

# Get the infos file of the host
./scripts/linux/get_infos.sh

docker build -f docker/Dockerfile_${1}:${2} -t registry.gitlab.inria.fr/adurey/fp-environnement-controle/${1}:${2} .
