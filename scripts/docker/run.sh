#!/bin/bash

# Allow connection to this device from this device. Need (not sure but I think) this to allow host and docker to be on same network
xhost local:root

docker run --rm -e DISPLAY=${DISPLAY} --volume="$HOME/.Xauthority:/root/.Xauthority:rw" --net=host registry.gitlab.inria.fr/adurey/fp-environnement-controle/${1} ${@:2}