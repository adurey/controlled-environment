#!/bin/bash

# Instal missing dependancy in some cases
#apt -qq -y install libdbus-glib-1-2

# Check for options i (run browser in incognito mode), t (timeout duration), p (profile folder), d (data folder), l (language), o (platform), a (information file)
incognito=""
time=10
rootFolder="../.."
dataFolder="${rootFolder}/firefox"
infoHostFile="${rootFolder}/infos.txt"
infoVirtualFile="${rootFolder}/infosVirtual.txt"
profile="default"
url=""
years=()
while getopts "it:p:o:u:y:" opt; do
  case $opt in
    i) incognito="-private-window"
      ;;
    t) time=$OPTARG
      ;;
    p) profile=$OPTARG
      ;;
    o) platform=$OPTARG
      ;;
    u) url=$OPTARG
      ;;
    y) IFS=',' read -r -a years <<< "$OPTARG"
      ;;
  esac
done

echo '-----------'
echo $url

# Get language
locale=$(locale | grep -oP "LANG=\K(.*)")
IFS='.' read -r -a locale <<< "$1"

language="en-US"

# Get platform
platform="linux-x86_64"

# Get options
versionsFile="${dataFolder}/versions.txt"
overrideFile="${dataFolder}/override.ini"
localSettingsFile="${dataFolder}/local-settings.js"
mozillaCfgFile="${dataFolder}/profiles/${profile}/mozilla.cfg"
extractFolder="${rootFolder}/firefox-tmp"
executable="./${extractFolder}/firefox/firefox"

baseURLpath="https://ftp.mozilla.org/pub/firefox/nightly"

# Create tmp dir
mkdir -p ${extractFolder}

# Format URL parameters
parameters=""
while IFS= read -r line
do
  parameters=${parameters}${line}"&"
done < "$infoHostFile"
while IFS= read -r line
do
  parameters=${parameters}${line}"&"
done < "$infoVirtualFile"
parameters=${parameters}"BROWSER_NAME=FIREFOX&BROWSER_PROFILE="${profile}"&"

# Clean any existing data on the ~/.mozilla/firefox folder
rm -rf ${rootDirectory}

# Get all versions and remove any beta or non major version
versions=()

while IFS= read -r version
do
  minorVersionNumber=$(echo ${version} | grep -oP "\.\K(.*)")
  beta=$(echo ${version} | grep -oP "\Kb")
  if [[ ${minorVersionNumber} = "0" && ${beta} = "" ]]; then
    versions+=(${version})
  fi
done < "$versionsFile"

# Get all years if y parameter is empty
if [[ ${#years[@]} = 0 ]]; then
  while read year; do
    years+=(${year})
  done < <(curl ${baseURLpath}/ --silent | grep -oP "<td><a href=[^>]*>\K[0-9]*")
fi

# Iterate on the folders ftp repository
for year in ${years[@]}
do
  while IFS= read -r month
  do
    while IFS= read -r nightlyInstance
    do
      version=$(curl https://ftp.mozilla.org/pub/firefox/nightly/${year}/${month}/${nightlyInstance} --silent | grep -oP "<td><a href=[^>]*>firefox-\K([0-9]+\.[0-9]+a[0-9]+)*" | head -1)
      majorVersion=$(echo $version | grep -oP "\K([0-9]|\.)*" | head -1)
      if [[ " ${versions[@]} " =~ " ${majorVersion} " ]]; then
        echo "Run for version ${version}"
        fullParameters="${parameters}BROWSER_VERSION=${version}-${nightlyInstance}"

        fileprefix="firefox-${majorVersion}a1.en-US.linux-x86_64"
#        firefox-81.0a1.en-US.linux-x86_64.tar.bz2
        filenameTar="${fileprefix}.tar"
        filename="${filenameTar}.bz2"

        fileURL="https://ftp.mozilla.org/pub/firefox/nightly/${year}/${month}/${nightlyInstance}${filename}"

        wget -q ${fileURL}
        echo "  File downloaded from ${fileURL}"
        bzip2 -d ${filename} > /dev/null
        echo "  File bzipped"

        tar -xvf ${filenameTar} -C ${extractFolder} > /dev/null
        echo "  File untar"
        rm ${fileprefix}*

        chmod +x ${executable}

        cp ${mozillaCfgFile} ${extractFolder}/firefox/
        cp ${localSettingsFile} ${extractFolder}/firefox/defaults/pref
        encodedParameters=`./url_encode.sh "$fullParameters"`

        echo "  Everything is ready. Starting the browser..."

        echo $url
        timeout ${time}s ${executable} -new-instance -no-remote -width 1024 -height 768 -override ${overrideFile} ${incognito} ${url}?${encodedParameters}
#  ${executable} -new-instance -no-remote -width 1024 -height 768 -override ${overrideFile} ${incognito} http://localhost:8092/environnement_controle?${encodedParameters}
        echo "  Browser closed after ${time} seconds "
        echo "  Cleaning..."
  # ./firefox/firefox -new-instance -setDefaultBrowser -P xp_mostPermissive http://spirals-adurey.lille.inria.fr/environnement_controle?${encodedParameters}
  # Clean
        sleep 2
        rm -rf ${rootFolder}
        rm -rf ${extractFolder}/firefox

      fi
    done < <(curl ${baseURLpath}/${year}/${month}/ --silent | grep -oP "<td><a href=[^>]*>\K[0-9]*-[0-9]*-[0-9]*-[0-9]*-[0-9]*-[0-9]*-mozilla-central/")
  done < <(curl ${baseURLpath}/${year}/ --silent | grep -oP "<td><a href=[^>]*>\K[0-9]*")
done

rm -rf ${extractFolder}