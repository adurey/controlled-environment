#!/bin/bash

incognito=""
torAdditionalTime=20
time=10
rootFolder="../.."
infoFile="${rootFolder}/infos.txt"
profile=""
url=""

while getopts "it:p:u:" opt; do
  case $opt in
    i) incognito="--inprivate"
      ;;
    t) time=$OPTARG
      ;;
    p) profile=$OPTARG
      ;;
    u) url=$OPTARG
  esac
done

time=$((time+torAdditionalTime))

sudo apt install tor
# Format URL parameters
parameters=""
while IFS= read -r line
do
  parameters=${parameters}${line}"&"
done < "$infoFile"
parameters=${parameters}"BROWSER_NAME=TOR&BROWSER_PROFILE="${profile}"&BROWSER_VERSION=$(tor --version)"

encodedParameters=`./url_encode.sh "$parameters"`

timeout ${time} tor ${incognito} --window-position=0,0 --window-size=1024,768 ${url}?${encodedParameters}