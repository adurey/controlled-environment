#!/bin/bash

# Check for options i (run browser in incognito mode), t (timeout duration), p (profile folder), d (data folder), l (language), o (platform), a (information file)
incognito=""
time=10
rootFolder="../.."
dataFolder="${rootFolder}/chrome"
infoFile="${rootFolder}/infos.txt"
profile=""
url=""
beta="false"
while getopts "iet:p:u:" opt; do
  case $opt in
    i) incognito="--incognito"
      ;;
    e) beta="true"
      ;;
    t) time=$OPTARG
      ;;
    p) profile=$OPTARG
      ;;
    u) url=$OPTARG
  esac
done

versionsFile="${dataFolder}/versions.txt"
betaVersionsFile="${dataFolder}/beta_versions.txt"


# wget -q --no-check-certificate https://raw.githubusercontent.com/Bugazelle/chromium-all-old-stable-versions/master/chromium.stable.json

downloadURLPrefix="http://dl.google.com/linux/chrome/deb/pool/main/g/google-chrome-"

# Format URL parameters
parameters=""
while IFS= read -r line
do
  parameters=${parameters}${line}"&"
done < "$infoFile"
parameters=${parameters}"BROWSER_NAME=CHROME&BROWSER_PROFILE="${profile}"&"

# Get versions (plus beta versions if required)
while IFS= read -r version
do
  versions+=(${version}"b")
  # Read all_versions file and add any matching version
  if [[ ${beta} == "true" ]]; then
    hat_major_version="^"$(echo $version | grep -oP "\K([0-9]+)" | head -1)
    while IFS= read -r betaVersion
    do
      grepResult=$(echo "${version}" | grep ${hat_major_version})
      if [[ ${grepResult} != "" && !(${versions[@]} =~ ${betaVersion}) ]]; then
        versions+=(${betaVersion}"b")
      fi
    done < "$betaVersionsFile"
  fi
done < "$betaVersionsFile"

for version in ${versions[@]}
do
  echo "Run for version ${version}"

  fullParameters=${parameters}"BROWSER_VERSION="${version}

  encodedParameters=`./url_encode.sh "$fullParameters"`

  isBeta=$(echo ${version} | grep -oP "b")

  if [[ ${isBeta} == "b" ]]; then
    version=$(echo ${version} | sed "s/b//")
    type="beta"
  else
    type="stable"
  fi
    urlToDownload=${downloadURLPrefix}
    filename="google-chrome-${type}_${version}-1_amd64.deb"
    urlToDownload="${urlToDownload}${type}/${filename}"

    wget ${urlToDownload}

    wget https://raw.githubusercontent.com/shivamgoyal15/Google-Chrome-Portable-maker-for-linux/master/GCPM
    if [[ ${type} == "stable" ]]; then
      chmod +x GCPM

      ./GCPM ${filename}


      # Export environment varibles to prevent Google API key messages
      export GOOGLE_API_KEY="no"
      export GOOGLE_DEFAULT_CLIENT_ID="no"
      export GOOGLE_DEFAULT_CLIENT_SECRET="no"

      # Add browser version to the parameter


      timeout ${time} ./ChromePortableGCPM/data/chrome ${incognito} --window-position=0,0 --window-size=1024,768 ${url}?${encodedParameters}

      rm GCPM
      rm -rf ChromePortableGCPM/



    else
      sudo dpkg -i ${filename}
      timeout ${time} google-chrome-beta ${incognito} --window-position=0,0 --window-size=1024,768 ${url}?${encodedParameters}
      sudo apt remove -y google-chrome-beta
    fi
      rm ${filename}
      sleep 2

#    sleep 2

#    rm -rf chrome-linux
done
