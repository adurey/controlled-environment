#!/bin/bash

incognito=""
time=10
rootFolder="../.."
infoFile="${rootFolder}/infos.txt"
profile=""
url=""

while getopts "it:p:u:" opt; do
  case $opt in
    i) incognito="--private"
      ;;
    t) time=$OPTARG
      ;;
    p) profile=$OPTARG
      ;;
    u) url=$OPTARG
  esac
done

sudo snap install opera

# Format URL parameters
parameters=""
while IFS= read -r line
do
  parameters=${parameters}${line}"&"
done < "$infoFile"
parameters=${parameters}"BROWSER_NAME=OPERA&BROWSER_PROFILE="${profile}"&BROWSER_VERSION=$(opera --version)"

encodedParameters=`./url_encode.sh "$parameters"`

timeout ${time} opera ${incognito} --window-position=0,0 --window-size=1024,768 ${url}?${encodedParameters}