#!/bin/bash

incognito=""
time=10
rootFolder="../.."
infoFile="${rootFolder}/infos.txt"
profile=""
url=""

while getopts "it:p:u:" opt; do
  case $opt in
    i) incognito="--inprivate"
      ;;
    t) time=$OPTARG
      ;;
    p) profile=$OPTARG
      ;;
    u) url=$OPTARG
  esac
done

curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
sudo install -o root -g root -m 644 microsoft.gpg /etc/apt/trusted.gpg.d/
sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/edge stable main" > /etc/apt/sources.list.d/microsoft-edge-dev.list'
sudo rm microsoft.gpg
## Install
sudo apt update
sudo apt install microsoft-edge-dev

# Format URL parameters
parameters=""
while IFS= read -r line
do
  parameters=${parameters}${line}"&"
done < "$infoFile"
parameters=${parameters}"BROWSER_NAME=EDGE&BROWSER_PROFILE="${profile}"&BROWSER_VERSION=$(microsoft-edge --version)"

encodedParameters=`./url_encode.sh "$parameters"`

timeout ${time} microsoft-edge ${incognito} --window-position=0,0 --window-size=1024,768 ${url}?${encodedParameters}