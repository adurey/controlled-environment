#!/bin/bash

incognito=""
time=10
rootFolder="../.."
infoHostFile="${rootFolder}/infos.txt"
infoVirtualFile="${rootFolder}/infosVirtual.txt"
profileFolders="${rootFolder}/brave/profiles"
profile="standard"
url=""

while getopts "it:p:u:" opt; do
  case $opt in
    i) incognito="--incognito"
      ;;
    t) time=$OPTARG
      ;;
    p) profile=$OPTARG
      ;;
    u) url=$OPTARG
  esac
done


# install brave
sudo apt install -y apt-transport-https curl gnupg jq
curl -s https://brave-browser-apt-release.s3.brave.com/brave-core.asc | sudo apt-key --keyring /etc/apt/trusted.gpg.d/brave-browser-release.gpg add -
echo "deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main" | sudo tee /etc/apt/sources.list.d/brave-browser-release.list
sudo apt update
sudo apt install -y brave-browser

# Replace fingerprinting protection by the on in the profile file
profileContent=`cat ${profileFolders}/${profile}.cfg`
fingerprintingProtection=`echo $profileContent | jq ".fingerprintingV2"`
bravePreferencesFilepath="${HOME}/.config/BraveSoftware/Brave-Browser/Default/Preferences"
jq --argjson protection "${fingerprintingProtection}" '.profile.content_settings.exceptions.fingerprintingV2 = $protection' $bravePreferencesFilepath

# Format URL parameters
parameters=""
while IFS= read -r line
do
  parameters=${parameters}${line}"&"
done < "$infoHostFile"
while IFS= read -r line
do
  parameters=${parameters}${line}"&"
done < "$infoVirtualFile"
parameters=${parameters}"BROWSER_NAME=BRAVE&BROWSER_PROFILE="${profile}"&BROWSER_VERSION=$(brave-browser --version)"

encodedParameters=`./url_encode.sh "$parameters"`

# echo $encodedParameters
timeout ${time} brave-browser  --new-window --window-position=0,0 --window-size=1024,768 ${incognito} ${url}?${encodedParameters} --no-sandbox