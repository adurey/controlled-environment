#!/bin/bash

# Instal missing dependancy in some cases
apt -qq -y install libdbus-glib-1-2

# Check for options i (run browser in incognito mode), t (timeout duration), p (profile folder), d (data folder), l (language), o (platform), a (information file)
incognito=""
time=10
rootFolder="../.."
dataFolder="${rootFolder}/firefox"
infoHostFile="${rootFolder}/infos.txt"
infoVirtualFile="${rootFolder}/infosVirtual.txt"
profile="default"
url=""
beta="false"
while getopts "eit:p:o:u:" opt; do
  case $opt in
    e) beta="true"
      ;;
    i) incognito="-private-window"
      ;;
    t) time=$OPTARG
      ;;
    p) profile=$OPTARG
      ;;
    o) platform=$OPTARG
      ;;
    u) url=$OPTARG
      ;;
  esac
done

# Get language
locale=$(locale | grep -oP "LANG=\K(.*)")
IFS='.' read -r -a locale <<< "$1"

language="en-US"

# Get platform
platform="linux-x86_64"

# Get options
allVersionsFile="${dataFolder}/all_versions.txt"
versionsFile="${dataFolder}/versions.txt"
overrideFile="${dataFolder}/override.ini"
localSettingsFile="${dataFolder}/local-settings.js"
mozillaCfgFile="${dataFolder}/profiles/${profile}/mozilla.cfg"
extractFolder="${rootFolder}/firefox-tmp"
executable="./${extractFolder}/firefox/firefox"

# Create tmp dir
mkdir -p ${extractFolder}

# Put profile in the right directory
rootDirectory=${HOME}"/.mozilla/firefox/"
if [[ $profile != "" ]]; then
  fullProfileName=$(ls -1 ${dataFolder}/profiles | grep $profile)
  fullProfilePath=${dataFolder}/profiles/${fullProfileName}
  mkdir -p ${rootDirectory}
  cp -r ${fullProfilePath}/* ${rootDirectory}
fi

# Format URL parameters
parameters=""
while IFS= read -r line
do
  parameters=${parameters}${line}"&"
done < "$infoHostFile"
while IFS= read -r line
do
  parameters=${parameters}${line}"&"
done < "$infoVirtualFile"
parameters=${parameters}"BROWSER_NAME=FIREFOX&BROWSER_PROFILE="${profile}"&"

# Clean any existing data on the ~/.mozilla/firefox folder
rm -rf ${rootDirectory}

# Get versions (plus beta versions if required)
versions=()

while IFS= read -r version
do
  versions+=(${version})
  grepResult=$(echo "${version}" | grep "b")
  if [[ ${beta} == "true" && ${grepResult} == "" ]]; then
    betaVersion="${version}b"
    # Read all_versions file and add any matching version
    while IFS= read -r allVersion
    do
      grepBetaResult=$(echo "${allVersion}" | grep "${betaVersion}")
      if [[ ${grepBetaResult} != "" ]]; then
        versions+=(${allVersion})
      fi
    done < "$allVersionsFile"

  fi
done < "$versionsFile"

# Iterate on versions
for version in ${versions[@]}
do
  echo "Run for version ${version}"
  fullParameters=${parameters}"BROWSER_VERSION="${version}

  firefoxVersion="firefox-${version}"
  filenameTar="${firefoxVersion}.tar"
  filename="${filenameTar}.bz2"

  fileURL="https://ftp.mozilla.org/pub/firefox/releases/${version}/${platform}/${language}/${filename}"

  wget -q ${fileURL}
  echo "  File downloaded from ${fileURL}"
  bzip2 -d ${filename} > /dev/null
  echo "  File bzipped"


  tar -xvf ${filenameTar} -C ${extractFolder} > /dev/null
  echo "  File untar"
  rm ${firefoxVersion}*

  chmod +x ${executable}

  cp ${mozillaCfgFile} ${extractFolder}/firefox/
  cp ${localSettingsFile} ${extractFolder}/firefox/defaults/pref
  encodedParameters=`./url_encode.sh "$fullParameters"`

  echo "  Everything is ready. Starting the browser..."

  echo $url
  timeout ${time}s ${executable} -new-instance -no-remote -width 1024 -height 768 -override ${overrideFile} ${incognito} ${url}?${encodedParameters}
#  ${executable} -new-instance -no-remote -width 1024 -height 768 -override ${overrideFile} ${incognito} http://localhost:8092/environnement_controle?${encodedParameters}
  echo "  Browser closed after ${time} seconds "
  echo "  Cleaning..."
  # ./firefox/firefox -new-instance -setDefaultBrowser -P xp_mostPermissive http://spirals-adurey.lille.inria.fr/environnement_controle?${encodedParameters}
  # Clean
  sleep 2
  rm -rf ${rootDirectory}
  rm -rf ${extractFolder}/firefox
done

rm -rf ${extractFolder}