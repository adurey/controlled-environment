#/bin/bash

virtualFilename="infosVirtual.txt"
virtualPrefix="VIRTUAL_"
virtualization=""

filename="infos.txt"
prefix=""
# d for docker, v for virtual machine
while getopts "vd" opt; do
  case $opt in
    v) filename=$virtualFilename
      prefix=$virtualPrefix
      virtualization="VIRTUAL_MACHINE"
      ;;
    d) filename=$virtualFilename
      prefix=$virtualPrefix
      virtualization="DOCKER"
  esac
done

rm -f ${filename}

echo $prefix"DEVICE_MANUFACTURER=$(dmidecode | grep -A3 '^System Information' | grep -oP 'Manufacturer:\ \K(.*)$')" >> ${filename}
echo $prefix"DEVICE_PRODUCT=$(dmidecode | grep -A3 '^System Information' | grep -oP 'Product Name:\ \K(.*)$')" >> ${filename}
count=1

# Get GPU manufacturers
while IFS= read -r manufacturer; do
  echo $prefix"GPU_MANUFACTURER_${count}=${manufacturer}" >> ${filename}
  count=$((count+1))
done < <(lshw -C display | grep -oP 'vendor: \K(.*)$')

count=1

# Get GPU models
while IFS= read -r model; do
  echo $prefix"GPU_MODEL_${count}=${model}" >> ${filename}
  count=$((count+1))
done < <(lshw -C display | grep -oP 'product: \K(.*)$')

count=1

# Get audio cards
apt -qq -y install alsa-utils
while IFS= read -r audio; do
  echo $prefix"AUDIO_CARD_MODEL_${count}=$(echo $audio | grep -oP '\[\K[^\]]*' | head -1)" >> ${filename}
  echo $prefix"AUDIO_CARD_DRIVER_${count}=$(echo $audio | grep -oP '\[\K[^\]]*' | tail -1)" >> ${filename}
  count=$((count+1))
done < <(arecord -l | grep -oP '^card .*')

# Get video input ?
ls -1 /dev/video*

if [[ $? == 0 ]]; then
  apt install v4l-utils
  while IFS= read -r video; do
    echo $prefix"VIDEO_INPUT_CARD_MODEL_${count}=$(echo video | grep -oP '\[\K[^\]]*' | head -1)" >> ${filename}
    echo $prefix"VIDEO_CARD_DRIVER_${count}=$(echo video | grep -oP '\[\K[^\]]*' | tail -1)" >> ${filename}
    count=$((count+1))
  done < <(ls -1 /dev/video*)
fi
echo $prefix"OS_NAME=$(cat /etc/os-release | grep -oP '^ID=\K(.*)$')" >> ${filename}
echo $prefix"OS_VERSION=$(cat /etc/os-release | grep -oP '^VERSION_ID="\K([^"]*)')" >> ${filename}
echo $prefix"PROCESSOR_MANUFACTURER=$(cat /proc/cpuinfo | grep -oPm 1 'vendor_id[[:space:]]+: \K(.*)$')" >> ${filename}
echo $prefix"PROCESSOR_MODEL=$(cat /proc/cpuinfo | grep -oPm 1 'model name[[:space:]]+: \K(.*)$')" >> ${filename}

while IFS= read -r locale; do
  echo $prefix${locale} >> ${filename}
done < <(locale)

echo $prefix"HOSTNAME=$(hostname)" >> ${filename}

if [[ $virtualization -ne "" ]]; then
  echo "VIRTUALIZATION=$virtualization" >> ${filename}
fi
# Screen size
# https://superuser.com/questions/603528/how-to-get-the-current-monitor-resolution-or-monitor-name-lvds-vga1-etc
# IFS='x' read -a size <<< $(xdpyinfo | grep dimensions | sed -r 's/^[^0-9]*([0-9]+x[0-9]+).*$/\1/')
# echo "SCREEN_WIDTH=${size[0]}" >> ${filename}
# echo "SCREEN_HEIGHT=${size[1]}" >> ${filename}
